package ru.t1.panasyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.user.UserLogoutRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserLogoutResponse;
import ru.t1.panasyuk.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    public final static String NAME = "user-logout";

    @NotNull
    private final String DESCRIPTION = "Logout current user.";

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        @NotNull final UserLogoutRequest request = new UserLogoutRequest(getToken());
        @NotNull final UserLogoutResponse response = getAuthEndpoint().logoutUser(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}