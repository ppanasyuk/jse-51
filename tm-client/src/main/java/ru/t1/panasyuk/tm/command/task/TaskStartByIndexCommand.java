package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.task.TaskStartByIndexRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskStartByIndexResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Start task by index.";

    @NotNull
    private static final String NAME = "task-start-by-index";

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber();
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken(), index);
        @NotNull final TaskStartByIndexResponse response = getTaskEndpoint().startTaskByIndex(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}