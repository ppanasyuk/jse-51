package ru.t1.panasyuk.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.data.DataXMLLoadFasterXMLRequest;

public final class DataXMLLoadFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from xml file.";

    @NotNull
    private static final String NAME = "data-load-xml-fasterxml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXMLLoadFasterXMLRequest request = new DataXMLLoadFasterXMLRequest(getToken());
        getDomainEndpoint().loadDataXMLFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
