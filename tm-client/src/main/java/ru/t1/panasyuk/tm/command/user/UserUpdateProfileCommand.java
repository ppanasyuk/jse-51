package ru.t1.panasyuk.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.user.UserUpdateProfileRequest;
import ru.t1.panasyuk.tm.dto.response.user.UserUpdateProfileResponse;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    private final String NAME = "user-update-profile";

    @NotNull
    private final String DESCRIPTION = "Update profile of current user.";

    @Override
    public void execute() {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.println("FIRST NAME:");
        @NotNull final String firstName = TerminalUtil.nextLine();
        System.out.println("LAST NAME:");
        @NotNull final String lastName = TerminalUtil.nextLine();
        System.out.println("MIDDLE NAME:");
        @NotNull final String middleName = TerminalUtil.nextLine();
        @NotNull final UserUpdateProfileRequest request = new UserUpdateProfileRequest(getToken(), firstName, lastName, middleName);
        @NotNull final UserUpdateProfileResponse response = getUserEndpoint().updateUserProfile(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}