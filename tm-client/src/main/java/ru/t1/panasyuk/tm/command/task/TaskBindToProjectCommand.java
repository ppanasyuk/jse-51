package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.task.TaskBindToProjectRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskBindToProjectResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class TaskBindToProjectCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Bind task to project.";

    @NotNull
    private static final String NAME = "task-bind-to-project";

    @Override
    public void execute() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(getToken(), projectId, taskId);
        @NotNull final TaskBindToProjectResponse response = getTaskEndpoint().bindTaskToProject(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}