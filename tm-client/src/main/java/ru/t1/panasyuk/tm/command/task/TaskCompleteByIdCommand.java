package ru.t1.panasyuk.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.task.TaskCompleteByIdRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskCompleteByIdResponse;
import ru.t1.panasyuk.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "Complete task by id.";

    @NotNull
    private static final String NAME = "task-complete-by-id";

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken(), id);
        @NotNull final TaskCompleteByIdResponse response = getTaskEndpoint().completeTaskById(request);
        if (!response.getSuccess()) System.out.println(response.getMessage());
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}