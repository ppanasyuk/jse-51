package ru.t1.panasyuk.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.data.DataYamlLoadFasterXMLRequest;

public final class DataYamlLoadFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from yaml file.";

    @NotNull
    private static final String NAME = "data-load-yaml-fasterxml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD YAML]");
        @NotNull final DataYamlLoadFasterXMLRequest request = new DataYamlLoadFasterXMLRequest(getToken());
        getDomainEndpoint().loadDataYamlFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
