package ru.t1.panasyuk.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.endpoint.IProjectEndpoint;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;

public abstract class AbstractProjectCommand extends AbstractCommand {

    @NotNull
    protected IProjectEndpoint getProjectEndpoint() {
        return getServiceLocator().getProjectEndpoint();
    }

    protected void showProject(@Nullable final ProjectDTO project) {
        if (project == null) return;
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        System.out.println("STATUS: " + Status.toName(project.getStatus()));
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}