package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.Domain;

public interface IDomainService {

    Domain getDomain() throws Exception;

    void setDomain(@Nullable Domain domain) throws Exception;

    void loadDataBackup();

    void saveDataBackup();

    void loadDataBase64();

    void saveDataBase64();

    void loadDataBinary();

    void saveDataBinary();

    void loadDataJsonFasterXML();

    void loadDataJsonJaxB();

    void saveDataJsonFasterXML();

    void saveDataJsonJaxB();

    void loadDataXMLFasterXML();

    void loadDataXMLJaxB();

    void saveDataXMLFasterXML();

    void saveDataXMLJaxB();

    void loadDataYamlFasterXML();

    void saveDataYamlFasterXML();

}