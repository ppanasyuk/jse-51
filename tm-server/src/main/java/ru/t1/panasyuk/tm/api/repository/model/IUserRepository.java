package ru.t1.panasyuk.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

}