package ru.t1.panasyuk.tm.listener;

import lombok.NoArgsConstructor;
import org.hibernate.event.spi.*;
import org.hibernate.persister.entity.EntityPersister;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.log.OperationType;
import ru.t1.panasyuk.tm.log.OperationEvent;

@NoArgsConstructor
public class EntityListener implements PostInsertEventListener, PostUpdateEventListener, PostDeleteEventListener {

    private JmsLoggerProducer jmsLoggerProducer;

    public EntityListener(@NotNull final JmsLoggerProducer jmsLoggerProducer) {
        this.jmsLoggerProducer = jmsLoggerProducer;
    }

    @Override
    public void onPostDelete(@NotNull final PostDeleteEvent event) {
        log(OperationType.DELETE, event.getEntity());
    }

    @Override
    public void onPostInsert(@NotNull final PostInsertEvent event) {
        log(OperationType.INSERT, event.getEntity());
    }

    @Override
    public void onPostUpdate(@NotNull final PostUpdateEvent event) {
        log(OperationType.UPDATE, event.getEntity());
    }

    @Override
    public boolean requiresPostCommitHanding(@NotNull final EntityPersister entityPersister) {
        return false;
    }

    private void log(final OperationType type, final Object entity) {
        jmsLoggerProducer.send(new OperationEvent(type, entity));
    }

}