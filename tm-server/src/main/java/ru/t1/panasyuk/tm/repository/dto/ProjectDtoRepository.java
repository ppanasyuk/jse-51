package ru.t1.panasyuk.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;

import javax.persistence.EntityManager;

public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDTO> implements IProjectDtoRepository {

    @Override
    protected @NotNull Class<ProjectDTO> getEntityClass() {
        return ProjectDTO.class;
    }

    public ProjectDtoRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

}