package ru.t1.panasyuk.tm.api.repository.model;

import ru.t1.panasyuk.tm.model.Project;

public interface IProjectRepository extends IUserOwnedRepository<Project> {
}