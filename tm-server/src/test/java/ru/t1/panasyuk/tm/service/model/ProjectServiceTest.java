package ru.t1.panasyuk.tm.service.model;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.api.service.model.IProjectService;
import ru.t1.panasyuk.tm.api.service.model.ITaskService;
import ru.t1.panasyuk.tm.api.service.model.IUserService;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.field.*;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование сервиса ProjectService")
public class ProjectServiceTest extends AbstractSchemeTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        userService = new UserService(propertyService, connectionService);
        projectList = new ArrayList<>();
        test = userService.create("TEST", "TEST", "TEST@TEST.ru");
        admin = userService.create("ADMIN", "ADMIN", "ADMIN@TEST.ru", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final Project project2 = projectService.create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final Project project3 = projectService.create(test.getId(), "Project 3", "Project for TEST 2");
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @After
    public void afterTest() throws Exception {
        userService.remove(admin);
        userService.remove(test);
    }

    @Test
    @DisplayName("Добавление проекта для пользователя")
    public void AddForUserTest() throws Exception {
        int expectedNumberOfEntries = projectService.getSize(test.getId()) + 1;
        @NotNull final Project project = new Project();
        project.setUser(test);
        project.setName("Test Add");
        project.setDescription("Test Add");
        projectService.add(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Добавление Null проекта для пользователя")
    public void AddNullForUserTest() throws Exception {
        int expectedNumberOfEntries = projectService.getSize(test.getId());
        @Nullable final Project project = projectService.add((Project) null);
        Assert.assertNull(project);
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Изменение статуса проекта по Id")
    public void changeProjectStatusByIdTest() throws Exception {
        @Nullable final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertNotNull(projects);
        for (@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), projectId, Status.IN_PROGRESS);
            Assert.assertNotNull(changedProject);
            changedProject = projectService.findOneById(test.getId(), projectId);
            Assert.assertNotNull(changedProject);
            Assert.assertEquals(Status.IN_PROGRESS, changedProject.getStatus());
        }
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Изменение статуса проекта по пустому Id")
    public void changeProjectStatusByIdProjectIdEmptyTestNegative() throws Exception {
        @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), "", Status.IN_PROGRESS);
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Изменение статуса проекта по Null Id")
    public void changeProjectStatusByIdNullProjectIdEmptyTestNegative() throws Exception {
        @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    @DisplayName("Изменение статуса проекта по Id на Null")
    public void changeProjectStatusByIdStatusIncorrectTestNegative() throws Exception {
        @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), "123", null);
    }

    @Test(expected = ProjectNotFoundException.class)
    @DisplayName("Изменение статуса несуществующего проекта по Id")
    public void changeProjectStatusByIdProjectNotFoundTestNegative() throws Exception {
        @Nullable Project changedProject = projectService.changeProjectStatusById(test.getId(), "123", Status.IN_PROGRESS);
    }

    @Test
    @DisplayName("Изменение статуса проекта по индексу")
    public void changeProjectStatusByIndexTest() throws Exception {
        @Nullable final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertNotNull(projects);
        for (int i = 0; i < projects.size(); i++) {
            @NotNull final Project project = projects.get(i);
            @NotNull final String projectId = project.getId();
            @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), i + 1, Status.IN_PROGRESS);
            Assert.assertNotNull(changedProject);
            changedProject = projectService.findOneById(test.getId(), changedProject.getId());
            Assert.assertNotNull(changedProject);
            Assert.assertEquals(Status.IN_PROGRESS, changedProject.getStatus());
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса проекта по Null индексу")
    public void changeProjectStatusByIndexIndexNullTestNegative() throws Exception {
        @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), null, Status.IN_PROGRESS);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса проекта по отрицательному индексу")
    public void changeProjectStatusByIndexIndexMinusTestNegative() throws Exception {
        @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), -1, Status.IN_PROGRESS);
    }

    @Test(expected = StatusIncorrectException.class)
    @DisplayName("Изменение статуса проекта по индексу на Null")
    public void changeProjectStatusByIIndexStatusIncorrectTestNegative() throws Exception {
        @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), 0, null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Изменение статуса проекта по индексу превышающему количество проектов")
    public void changeProjectStatusByIndexIndexIncorrectTestNegative() throws Exception {
        @Nullable Project changedProject = projectService.changeProjectStatusByIndex(test.getId(), 100, Status.IN_PROGRESS);
    }

    @Test
    @DisplayName("Удалить все проекты для пользователя")
    public void clearForUserTest() throws Exception {
        int expectedNumberOfEntries = 0;
        Assert.assertTrue(projectService.getSize(test.getId()) > 0);
        projectService.clear(test.getId());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Создать проект по имени и описанию")
    public void createTest() throws Exception {
        int expectedNumberOfEntries = projectService.getSize(test.getId()) + 1;
        @NotNull final String name = "Project name";
        @NotNull final String description = "Project Description";
        @Nullable Project createdProject = projectService.create(test.getId(), name, description);
        @NotNull final String projectId = createdProject.getId();
        Assert.assertNotNull(createdProject);
        createdProject = projectService.findOneById(test.getId(), projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(description, createdProject.getDescription());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test
    @DisplayName("Создать проект по имени")
    public void createByNameTest() throws Exception {
        int expectedNumberOfEntries = projectService.getSize(test.getId()) + 1;
        @NotNull final String name = "Project name";
        @Nullable Project createdProject = projectService.create(test.getId(), name);
        @NotNull final String projectId = createdProject.getId();
        Assert.assertNotNull(createdProject);
        createdProject = projectService.findOneById(test.getId(), projectId);
        Assert.assertNotNull(createdProject);
        Assert.assertEquals(name, createdProject.getName());
        Assert.assertEquals(expectedNumberOfEntries, projectService.getSize(test.getId()));
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создать проект по имени и описанию с пустым именем")
    public void createNameEmptyTestNegative() throws Exception {
        projectService.create(test.getId(), "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создать проект по имени и описанию с Null именем")
    public void createNullNameEmptyTestNegative() throws Exception {
        projectService.create(test.getId(), null, "description");
    }

    @Test(expected = DescriptionEmptyException.class)
    @DisplayName("Создать проект по имени и описанию с пустым описанием")
    public void createDescriptionEmptyTestNegative() throws Exception {
        projectService.create(test.getId(), "name", "");
    }

    @Test(expected = DescriptionEmptyException.class)
    @DisplayName("Создать проект по имени и описанию с Null описанием")
    public void createNullDescriptionEmptyTestNegative() throws Exception {
        projectService.create(test.getId(), "name", null);
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создать проект по имени с пустым именем")
    public void createByNameNameEmptyTestNegative() throws Exception {
        projectService.create(test.getId(), "");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Создать проект по имени с Null именем")
    public void createByNameNullNameEmptyTestNegative() throws Exception {
        projectService.create(test.getId(), null);
    }

    @Test
    @DisplayName("Проверить существование проекта по Id для пользователя")
    public void existByIdTrueForUserTest() throws Exception {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            final boolean isExist = projectService.existsById(test.getId(), project.getId());
            Assert.assertTrue(isExist);
        }
    }

    @Test
    @DisplayName("Проверить отсутствие проекта по Id для пользователя")
    public void existByIdFalseUserTest() throws Exception {
        final boolean isExist = projectService.existsById("45", "123321");
        Assert.assertFalse(isExist);
    }

    @Test
    @DisplayName("Найти все проекты")
    public void findAllTest() throws Exception {
        @Nullable final List<Project> projects = projectService.findAll();
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
    }

    @Test
    @DisplayName("Найти все проекты для пользователя")
    public void findAllForUserTest() throws Exception {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
    }

    @Test
    @DisplayName("Найти все проекты по копаратору для пользователя")
    public void findAllWithComparatorForUserTest() throws Exception {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable Comparator<Project> comparator = Sort.BY_NAME.getComparator();
        @Nullable List<Project> projects = projectService.findAll(test.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        comparator = Sort.BY_CREATED.getComparator();
        projects = projectService.findAll(test.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        comparator = Sort.BY_STATUS.getComparator();
        projects = projectService.findAll(test.getId(), comparator);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
    }

    @Test
    @DisplayName("Найти все проекты с сортировкой для пользователя")
    public void findAllWithSortForUserTest() throws Exception {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable List<Project> projects = projectService.findAll(test.getId(), Sort.BY_NAME);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        projects = projectService.findAll(test.getId(), Sort.BY_CREATED);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        projects = projectService.findAll(test.getId(), Sort.BY_STATUS);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
        @Nullable final Sort sort = null;
        projects = projectService.findAll(test.getId(), sort);
        Assert.assertNotNull(projects);
        Assert.assertEquals(projectsForTestUser.size(), projects.size());
    }

    @Test
    @DisplayName("Найти проект по Id для пользователя")
    public void findOneByIdForUserTest() throws Exception {
        @Nullable Project foundProject;
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            foundProject = projectService.findOneById(test.getId(), project.getId());
            Assert.assertNotNull(foundProject);
        }
    }

    @Test
    @DisplayName("Найти проект по Null Id для пользователя")
    public void findOneByIdNullForUserTest() throws Exception {
        @Nullable final Project foundProject = projectService.findOneById(test.getId(), null);
        Assert.assertNull(foundProject);
    }

    @Test
    @DisplayName("Найти проект по индексу для пользователя")
    public void findOneByIndexForUserTest() throws Exception {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 1; i <= projectsForTestUser.size(); i++) {
            @Nullable final Project project = projectService.findOneByIndex(test.getId(), i);
            Assert.assertNotNull(project);
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти проект по индексу превышающему количество проектов для пользователя")
    public void findOneByIndexForUserIndexIncorrectNegative() throws Exception {
        int index = projectService.getSize(test.getId()) + 1;
        @Nullable final Project project = projectService.findOneByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти проект по Null индексу для пользователя")
    public void findOneByIndexForUserNullIndexIncorrectNegative() throws Exception {
        @Nullable final Project project = projectService.findOneByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Найти проект по отрицательному индексу для пользователя")
    public void findOneByIndexForUserMinusIndexIncorrectNegative() throws Exception {
        @Nullable final Project project = projectService.findOneByIndex(test.getId(), -1);
    }

    @Test
    @DisplayName("Получить количество проектов для пользователя")
    public void getSizeForUserTest() throws Exception {
        int expectedSize = (int) projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .count();
        int actualSize = projectService.getSize(test.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удаление проекта по Id для пользователя")
    public void removeByIdForUserTest() throws Exception {
        @NotNull final List<Project> projectsForTestUser = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        for (@NotNull final Project project : projectsForTestUser) {
            @NotNull final String projectId = project.getId();
            @Nullable final Project deletedProject = projectService.removeById(test.getId(), projectId);
            Assert.assertNotNull(deletedProject);
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(test.getId(), projectId);
            Assert.assertNull(deletedProjectInRepository);
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удаление проекта по Null Id для пользователя")
    public void removeByIdForUserIdNullTestNegative() throws Exception {
        projectService.removeById(test.getId(), null);
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Удаление проекта по пустому Id для пользователя")
    public void removeByIdForUserIdEmptyTestNegative() throws Exception {
        projectService.removeById(test.getId(), "");
    }

    @Test(expected = EntityNotFoundException.class)
    @DisplayName("Удаление несуществующего проекта по Id для пользователя")
    public void removeByIdForUserEntityNotFoundTestNegative() throws Exception {
        projectService.removeById(test.getId(), "123321");
    }

    @Test
    @DisplayName("Удаление проекта по индексу для пользователя")
    public void removeByIndexForUserTest() throws Exception {
        int index = (int) projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .count();
        while (index > 0) {
            @Nullable final Project deletedProject = projectService.removeByIndex(test.getId(), index);
            Assert.assertNotNull(deletedProject);
            @NotNull final String projectId = deletedProject.getId();
            @Nullable final Project deletedProjectInRepository = projectService.findOneById(test.getId(), projectId);
            Assert.assertNull(deletedProjectInRepository);
            index--;
        }
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удаление проекта по индексу превышающему количество пректов для пользователя")
    public void removeByIndexForUserIndexIncorrectTestNegative() throws Exception {
        int index = projectList.size() + 1;
        projectService.removeByIndex(test.getId(), index);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удаление проекта по Null индексу для пользователя")
    public void removeByIndexNullForUserIndexIncorrectTestNegative() throws Exception {
        projectService.removeByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удаление проекта по отрицательному индексу для пользователя")
    public void removeByIndexMinusForUserIndexIncorrectTestNegative() throws Exception {
        projectService.removeByIndex(test.getId(), -1);
    }

    @Test
    @DisplayName("Обновление проекта по Id")
    public void updateByIdTest() throws Exception {
        @Nullable final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertNotNull(projects);
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 0;
        for (@NotNull final Project project : projects) {
            @NotNull final String projectId = project.getId();
            name = "name " + index;
            description = "description" + index;
            @Nullable Project updatedProject = projectService.updateById(test.getId(), projectId, name, description);
            Assert.assertNotNull(updatedProject);
            updatedProject = projectService.findOneById(test.getId(), projectId);
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals(name, updatedProject.getName());
            Assert.assertEquals(description, updatedProject.getDescription());
            index++;
        }
    }

    @Test
    @DisplayName("Обновление проекта по индексу")
    public void updateByIndexTest() throws Exception {
        @Nullable final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertNotNull(projects);
        @NotNull String name = "";
        @NotNull String description = "";
        int index = 1;
        for (@NotNull final Project project : projects) {
            name = "name " + index;
            description = "description" + index;
            @Nullable Project updatedProject = projectService.updateByIndex(test.getId(), index, name, description);
            Assert.assertNotNull(updatedProject);
            updatedProject = projectService.findOneById(test.getId(), updatedProject.getId());
            Assert.assertNotNull(updatedProject);
            Assert.assertEquals(name, updatedProject.getName());
            Assert.assertEquals(description, updatedProject.getDescription());
            index++;
        }
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновление проекта по пустому Id")
    public void UpdateByIdIdEmptyTestNegative() throws Exception {
        projectService.updateById(test.getId(), "", "name", "description");
    }

    @Test(expected = IdEmptyException.class)
    @DisplayName("Обновление проекта по Null Id")
    public void UpdateByIdNullIdEmptyTestNegative() throws Exception {
        projectService.updateById(test.getId(), null, "name", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновление проекта пустым именем по Id")
    public void UpdateByIdNameEmptyTestNegative() throws Exception {
        projectService.updateById(test.getId(), "id", "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновление проекта Null именем по Id")
    public void UpdateByIdNullNameEmptyTestNegative() throws Exception {
        projectService.updateById(test.getId(), "id", null, "description");
    }

    @Test(expected = ProjectNotFoundException.class)
    @DisplayName("Обновление несуществующего проекта по Id")
    public void UpdateByIdProjectNotFoundTestNegative() throws Exception {
        projectService.updateById(test.getId(), "123", "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновление проекта по Null индексу")
    public void UpdateByIndexIndexNullTestNegative() throws Exception {
        projectService.updateByIndex(test.getId(), null, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновление проекта по отрицательному индексу")
    public void UpdateByIndexMinusTestNegative() throws Exception {
        projectService.updateByIndex(test.getId(), -1, "name", "description");
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Обновление проекта по индексу превышающему количество проектов")
    public void UpdateByIndexIndexIncorrectTestNegative() throws Exception {
        projectService.updateByIndex(test.getId(), 100, "", "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновление проекта Null именем по индексу")
    public void UpdateByIndexNullNameEmptyTestNegative() throws Exception {
        projectService.updateByIndex(test.getId(), 0, null, "description");
    }

    @Test(expected = NameEmptyException.class)
    @DisplayName("Обновление проекта пустым именем по индексу")
    public void UpdateByIndexNameEmptyTestNegative() throws Exception {
        projectService.updateByIndex(test.getId(), 0, "", "description");
    }

}