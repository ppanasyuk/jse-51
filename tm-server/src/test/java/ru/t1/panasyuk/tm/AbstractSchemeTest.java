package ru.t1.panasyuk.tm;

import liquibase.Liquibase;
import liquibase.database.Database;
import liquibase.database.DatabaseFactory;
import liquibase.database.jvm.JdbcConnection;
import liquibase.exception.DatabaseException;
import liquibase.exception.LiquibaseException;
import liquibase.resource.ClassLoaderResourceAccessor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import ru.t1.panasyuk.tm.api.service.IConnectionService;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public abstract class AbstractSchemeTest {

    @NotNull
    protected static Liquibase liquibase;

    @NotNull
    protected static IConnectionService connectionService;

    @BeforeClass
    public static void beforeClass() throws SQLException, IOException, LiquibaseException {
        liquibase = configDBByLiquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
    }

    @AfterClass
    public static void afterClass() throws LiquibaseException {
        liquibase.close();
        connectionService.close();
    }

    @NotNull
    protected static Liquibase configDBByLiquibase(@NotNull final String filename)
            throws IOException, SQLException, DatabaseException {
        @NotNull final ClassLoaderResourceAccessor accessor = new ClassLoaderResourceAccessor();
        @NotNull final Properties properties = new Properties();
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream("liquibase.properties");
        properties.load(inputStream);
        @NotNull final Connection connection = getConnection(properties);
        @NotNull final JdbcConnection jdbcConnection = new JdbcConnection(connection);
        @NotNull final Database database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(jdbcConnection);
        return new Liquibase(filename, accessor, database);
    }

    @NotNull
    private static Connection getConnection(@NotNull final Properties properties) throws SQLException {
        return DriverManager.getConnection(
                properties.getProperty("url"),
                properties.getProperty("username"),
                properties.getProperty("password")
        );
    }

}