package ru.t1.panasyuk.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.service.*;
import ru.t1.panasyuk.tm.api.service.dto.IAuthDtoService;
import ru.t1.panasyuk.tm.api.service.dto.IProjectTaskDtoService;
import ru.t1.panasyuk.tm.api.service.dto.ISessionDtoService;
import ru.t1.panasyuk.tm.api.service.dto.IUserDtoService;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.field.LoginEmptyException;
import ru.t1.panasyuk.tm.exception.field.PasswordEmptyException;
import ru.t1.panasyuk.tm.exception.system.AccessDeniedException;
import ru.t1.panasyuk.tm.exception.system.AuthenticationException;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.dto.model.UserDTO;
import ru.t1.panasyuk.tm.service.dto.AuthDtoService;
import ru.t1.panasyuk.tm.service.dto.ProjectTaskDtoService;
import ru.t1.panasyuk.tm.service.dto.SessionDtoService;
import ru.t1.panasyuk.tm.service.dto.UserDtoService;
import ru.t1.panasyuk.tm.util.CryptUtil;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@DisplayName("Тестирование сервиса AuthService")
public class AuthServiceTest extends AbstractSchemeTest {

    @NotNull
    private IAuthDtoService authService;

    @NotNull
    private ISessionDtoService sessionService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private IPropertyService propertyService;

    @NotNull
    private List<UserDTO> userList;

    @NotNull
    private List<SessionDTO> sessionList;

    @NotNull
    private UserDTO user;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        propertyService = new PropertyService();
        @NotNull final IProjectTaskDtoService projectTaskService = new ProjectTaskDtoService(connectionService);
        userService = new UserDtoService(propertyService, connectionService, projectTaskService);
        sessionService = new SessionDtoService(connectionService);
        authService = new AuthDtoService(propertyService, userService, sessionService);
        @NotNull final UserDTO test = userService.create("TEST", "TEST", "TEST@TEST.ru");
        @NotNull final UserDTO admin = userService.create("ADMIN", "ADMIN", "ADMIN@TEST.ru", Role.ADMIN);
        user = userService.create("USER", "USER", "USET@TEST.ru", Role.ADMIN);
        userList = new ArrayList<>();
        userList.add(test);
        userList.add(admin);
        userList.add(user);
        @NotNull final SessionDTO session1 = new SessionDTO();
        session1.setUserId(test.getId());
        session1.setRole(test.getRole());
        sessionService.add(test.getId(), session1);
        @NotNull final SessionDTO session2 = new SessionDTO();
        session2.setUserId(admin.getId());
        session2.setRole(admin.getRole());
        sessionService.add(admin.getId(), session2);
        @NotNull final SessionDTO session3 = new SessionDTO();
        session3.setUserId(user.getId());
        session3.setRole(user.getRole());
        sessionService.add(user.getId(), session3);
        sessionList = new ArrayList<>();
        sessionList.add(session1);
        sessionList.add(session2);
        sessionList.add(session3);
    }

    @After
    public void afterTest() throws Exception {
        for (@NotNull final UserDTO user : userList) {
            sessionService.clear(user.getId());
        }
        for (@NotNull final UserDTO user : userList) {
            userService.remove(user);
        }
    }

    @Test
    @DisplayName("Проверка логина и пароля пользователей")
    public void checkTest() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final UserDTO testUser = authService.check(user.getLogin(), user.getLogin());
            Assert.assertNotNull(testUser);
        }
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Проверка логина и пароля пользователяс пустым логином")
    public void checkLoginEmptyTestNegative() throws Exception {
        @Nullable final UserDTO testUser = authService.check("", user.getLogin());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Проверка логина и пароля пользователя с Null логином")
    public void checkLoginNullTestNegative() throws Exception {
        @Nullable final UserDTO testUser = authService.check(null, user.getLogin());
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Проверка логина и пароля пользователя с пустым паролем")
    public void checkPasswordEmptyTestNegative() throws Exception {
        @Nullable final UserDTO testUser = authService.check(user.getLogin(), "");
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Проверка логина и пароля пользователя с Null паролем")
    public void checkPasswordNullTestNegative() throws Exception {
        @Nullable final UserDTO testUser = authService.check(user.getLogin(), null);
    }

    @Test(expected = AuthenticationException.class)
    @DisplayName("Проверка логина и пароля пользователя с некорректным логином")
    public void checkUserNotFoundTestNegative() throws Exception {
        @Nullable final UserDTO testUser = authService.check("ABYRVALG", "PASS");
    }

    @Test(expected = AuthenticationException.class)
    @DisplayName("Проверка логина и пароля заблокированного пользователя")
    public void checkUserLockedTestNegative() throws Exception {
        userService.lockUserByLogin(user.getLogin());
        @Nullable final UserDTO testUser = authService.check(user.getLogin(), user.getLogin());
    }

    @Test(expected = AuthenticationException.class)
    @DisplayName("Проверка логина и пароля пользователя с некорректным паролем")
    public void checkWrongPasswordTestNegative() throws Exception {
        @Nullable final UserDTO testUser = authService.check(user.getLogin(), "PASS");
    }

    @Test
    @DisplayName("Выполнение аутентификации пользователя")
    public void loginTest() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
        }
    }

    @DisplayName("Выполнение аутентификации пользователя с пустым логином")
    @Test(expected = LoginEmptyException.class)
    public void loginLoginEmptyTestNegative() throws Exception {
        @Nullable final String token = authService.login("", user.getLogin());
    }

    @Test(expected = LoginEmptyException.class)
    @DisplayName("Выполнение аутентификации пользователя с Null логином")
    public void loginLoginNullTestNegative() throws Exception {
        @Nullable final String token = authService.login(null, user.getLogin());
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Выполнение аутентификации пользователя с пустым паролем")
    public void loginPasswordEmptyTestNegative() throws Exception {
        @Nullable final String token = authService.login(user.getLogin(), "");
    }

    @Test(expected = PasswordEmptyException.class)
    @DisplayName("Выполнение аутентификации пользователя с Null паролем")
    public void loginPasswordNullTestNegative() throws Exception {
        @Nullable final String token = authService.login(user.getLogin(), null);
    }

    @Test(expected = AuthenticationException.class)
    @DisplayName("Выполнение аутентификации пользователя с некорректным логином")
    public void loginUserNotFoundTestNegative() throws Exception {
        @Nullable final String token = authService.login("ABYRVALG", "PASS");
    }

    @Test(expected = AuthenticationException.class)
    @DisplayName("Выполнение аутентификации заблокированного пользователя")
    public void loginUserLockedTestNegative() throws Exception {
        userService.lockUserByLogin(user.getLogin());
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
    }

    @Test(expected = AuthenticationException.class)
    @DisplayName("Выполнение аутентификации пользователя с некорректным паролем")
    public void loginWrongPasswordTestNegative() throws Exception {
        @Nullable final String token = authService.login(user.getLogin(), "PASS");
    }

    @Test
    @DisplayName("Выполнение логаута пользователя")
    public void logoutTest() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
            @NotNull final SessionDTO validSession = authService.validateToken(token);
            Assert.assertNotNull(validSession);
            Assert.assertNotNull(validSession.getUserId());
            boolean isSessionExists = sessionService.existsById(validSession.getUserId(), validSession.getId());
            Assert.assertTrue(isSessionExists);
            authService.logout(token);
            isSessionExists = sessionService.existsById(validSession.getUserId(), validSession.getId());
            Assert.assertFalse(isSessionExists);
            authService.logout(token);
        }
    }

    @Test(expected = AccessDeniedException.class)
    @DisplayName("Выполнение логаута пользователя с пустым токеном")
    public void logoutTokenNullTestNegative() {
        authService.logout(null);
    }

    @Test(expected = AccessDeniedException.class)
    @DisplayName("Выполнение логаута пользователя с некорректным токеном")
    public void logoutWrongTokenTestNegative() {
        @NotNull final SessionDTO validSession = authService.validateToken("123456");
    }

    @Test
    @DisplayName("Валидация токена")
    public void validateTokenTest() throws Exception {
        Assert.assertTrue(userList.size() > 0);
        for (@NotNull final UserDTO user : userList) {
            @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
            Assert.assertNotNull(token);
            Assert.assertFalse(token.isEmpty());
            @NotNull final SessionDTO validSession = authService.validateToken(token);
            Assert.assertNotNull(validSession);
            Assert.assertNotNull(validSession.getUserId());
            boolean isSessionExists = sessionService.existsById(validSession.getUserId(), validSession.getId());
            Assert.assertTrue(isSessionExists);
            authService.logout(token);
        }
    }

    @Test(expected = AccessDeniedException.class)
    @DisplayName("Валидация Null токена")
    public void validateTokenTokenNullTestNegative() {
        @NotNull final SessionDTO validSession = authService.validateToken(null);
    }

    @Test(expected = AccessDeniedException.class)
    @DisplayName("Валидация некорректного токена")
    public void validateTokenWrongTokenTestNegative() {
        @NotNull final SessionDTO validSession = authService.validateToken("123456");
    }

    @Test(expected = AccessDeniedException.class)
    @DisplayName("Проверка валидности токена с просроченной сессисей")
    public void validateTokenExpiredSessionTestNegative() throws Exception {
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        @NotNull final SessionDTO validSession = authService.validateToken(token);
        Assert.assertNotNull(validSession);
        @NotNull final Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.DATE, -30);
        @NotNull final Date dateBefore = cal.getTime();
        validSession.setCreated(dateBefore);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String tokenForMapper = objectMapper.writeValueAsString(validSession);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String invalidToken = CryptUtil.encrypt(tokenForMapper, sessionKey);
        @NotNull final SessionDTO invalidSession = authService.validateToken(invalidToken);
    }

    @Test(expected = AccessDeniedException.class)
    @DisplayName("Проверка валидности токена с несуществующей сессией")
    public void validateTokenSessionNotExistTestNegative() throws Exception {
        @Nullable final String token = authService.login(user.getLogin(), user.getLogin());
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        @NotNull final SessionDTO validSession = authService.validateToken(token);
        Assert.assertNotNull(validSession);
        @NotNull final String sessionId = validSession.getId();
        @Nullable final String userId = validSession.getUserId();
        Assert.assertNotNull(userId);
        sessionService.removeById(userId, sessionId);
        @NotNull final SessionDTO invalidSession = authService.validateToken(token);
    }

    @Test
    @DisplayName("Деактивация сессии")
    public void invalidateTest() throws Exception {
        @Nullable final List<SessionDTO> sessionList = sessionService.findAll();
        Assert.assertNotNull(sessionList);
        Assert.assertTrue(sessionList.size() > 0);
        for (@NotNull final SessionDTO session : sessionList) {
            authService.invalidate(session);
        }
        Assert.assertEquals(0, sessionService.findAll().size());
    }

    @Test
    @DisplayName("Деактивация Null сессии")
    public void InvalidateNullTest() throws Exception {
        int expectedNumberOfEntries = sessionService.findAll().size();
        authService.invalidate(null);
        int actualNumberOfEntries = sessionService.findAll().size();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("регистрация пользователя")
    public void registryTest() throws Exception {
        @NotNull final String login = "LOGIN";
        @NotNull final String password = "PASS";
        @NotNull final String email = "EMAIL";
        @NotNull final UserDTO user = authService.registry(login, password, email);
        Assert.assertNotNull(user);
        @Nullable final UserDTO newUser = userService.findOneById(user.getId());
        Assert.assertNotNull(newUser);
        Assert.assertEquals(login, newUser.getLogin());
        Assert.assertEquals(email, newUser.getEmail());
        @Nullable final String token = authService.login(newUser.getLogin(), "PASS");
        Assert.assertNotNull(token);
        Assert.assertFalse(token.isEmpty());
        authService.logout(token);
        userService.remove(newUser);
    }

}