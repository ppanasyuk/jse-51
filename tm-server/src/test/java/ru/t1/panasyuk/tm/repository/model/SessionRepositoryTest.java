package ru.t1.panasyuk.tm.repository.model;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.model.ISessionRepository;
import ru.t1.panasyuk.tm.api.repository.model.IUserRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.Session;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование репозитория сессий на графах")
public class SessionRepositoryTest extends AbstractSchemeTest {

    private final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private User testUser1;

    @NotNull
    private User testUser2;

    @NotNull
    private List<Session> sessionList;

    @NotNull
    private ISessionRepository sessionRepository;

    @NotNull
    private static EntityManager entityManager;

    @BeforeClass
    public static void initConnection() throws SQLException, IOException, LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initRepository() {
        sessionList = new ArrayList<>();
        entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IUserRepository userRepository = new UserRepository(entityManager);
        testUser1 = new User();
        testUser2 = new User();
        userRepository.add(testUser1);
        userRepository.add(testUser2);
        sessionRepository = new SessionRepository(entityManager);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Session session = new Session();
            if (i < 5) session.setUser(testUser1);
            else session.setUser(testUser2);
            session.setRole(Role.USUAL);
            sessionList.add(session);
            sessionRepository.add(session);
        }
    }

    @After
    public void afterTest() {
        entityManager.getTransaction().rollback();
        entityManager.close();
    }

    @Test
    @DisplayName("Добавить сессию")
    public void addTest() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 1;
        @NotNull final Session session = new Session();
        session.setRole(Role.USUAL);
        session.setUser(testUser1);
        sessionRepository.add(session);
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize());
        @Nullable final Session createdSession = sessionRepository.findOneById(session.getId());
        Assert.assertNotNull(createdSession);
        Assert.assertEquals(testUser1, createdSession.getUser());
    }

    @Test
    @DisplayName("Добавить список сессий")
    public void addAllTest() {
        int expectedNumberOfEntries = sessionRepository.getSize() + 2;
        @NotNull final List<Session> sessions = new ArrayList<>();
        @NotNull final Session firstSession = new Session();
        firstSession.setRole(Role.USUAL);
        sessions.add(firstSession);
        @NotNull final Session secondSession = new Session();
        secondSession.setRole(Role.USUAL);
        sessions.add(secondSession);
        @NotNull final Collection<Session> addedSessions = sessionRepository.add(sessions);
        Assert.assertTrue(addedSessions.size() > 0);
        int actualNumberOfEntries = sessionRepository.getSize();
        Assert.assertEquals(expectedNumberOfEntries, actualNumberOfEntries);
    }

    @Test
    @DisplayName("Удалить все сессии для пользователя")
    public void clearForUserTest() {
        int expectedNumberOfEntries = 0;
        sessionRepository.clear(testUser1.getId());
        Assert.assertEquals(expectedNumberOfEntries, sessionRepository.getSize(testUser1.getId()));
    }

    @Test
    @DisplayName("Поиск всех сессий")
    public void findAllTest() throws Exception {
        @Nullable final List<Session> sessions = sessionRepository.findAll();
        Assert.assertNotNull(sessions);
        Assert.assertTrue(sessions.size() > 0);
    }

    @Test
    @DisplayName("Поиск всех сессий для пользователя")
    public void findAllForUserTest() {
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        @Nullable final List<Session> sessions = sessionRepository.findAll(testUser1.getId());
        Assert.assertNotNull(sessions);
        Assert.assertEquals(sessionListForUser.size(), sessions.size());
    }

    @Test
    @DisplayName("Поиск сессии по Id")
    public void findOneByIdTest() {
        @Nullable Session session;
        for (int i = 0; i < sessionList.size(); i++) {
            session = sessionList.get(i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session foundSession = sessionRepository.findOneById(sessionId);
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null Id")
    public void findOneByIdNullTest() {
        @Nullable final Session foundSession = sessionRepository.findOneById("qwerty");
        Assert.assertNull(foundSession);
        @Nullable final Session foundSessionNull = sessionRepository.findOneById(null);
        Assert.assertNull(foundSessionNull);
    }

    @Test
    @DisplayName("Поиск сессии по Id для пользователя")
    public void findOneByIdForUserTest() {
        @Nullable Session session;
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionListForUser.size(); i++) {
            session = sessionRepository.findOneByIndex(testUser1.getId(), i);
            Assert.assertNotNull(session);
            @NotNull final String sessionId = session.getId();
            @Nullable final Session foundSession = sessionRepository.findOneById(testUser1.getId(), sessionId);
            Assert.assertNotNull(foundSession);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null Id для пользователя")
    public void findOneByIdNullForUserTest() {
        @Nullable final Session foundSession = sessionRepository.findOneById(testUser1.getId(), "qwerty");
        Assert.assertNull(foundSession);
        @Nullable final Session foundSessionNull = sessionRepository.findOneById(testUser1.getId(), null);
        Assert.assertNull(foundSessionNull);
    }

    @Test
    @DisplayName("Поиск сессии по индексу")
    public void findOneByIndexTest() {
        @Nullable final Session session = sessionRepository.findOneByIndex(1);
        Assert.assertNotNull(session);
    }

    @Test
    @DisplayName("Поиск сессии по Null индексу")
    public void findOneByIndexNullTest() {
        @Nullable final Session session = sessionRepository.findOneByIndex(null);
        Assert.assertNull(session);
    }

    @Test
    @DisplayName("Поиск сессии по индексу для пользователя")
    public void findOneByIndexForUserTest() {
        @NotNull List<Session> sessionListForUser = sessionList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .collect(Collectors.toList());
        for (int i = 1; i <= sessionListForUser.size(); i++) {
            @Nullable final Session session = sessionRepository.findOneByIndex(testUser1.getId(), i);
            Assert.assertNotNull(session);
        }
    }

    @Test
    @DisplayName("Поиск сессии по Null индексу для пользователя")
    public void findOneByIndexNullForUserText() {
        @Nullable final Session session = sessionRepository.findOneByIndex(testUser1.getId(), null);
        Assert.assertNull(session);
    }

    @Test
    @DisplayName("Получение количества сессий")
    public void getSizeTest() throws Exception {
        int actualSize = sessionRepository.getSize();
        Assert.assertTrue(actualSize > 0);
    }

    @Test
    @DisplayName("Получение количества сессий для пользователя")
    public void getSizeForUserTest() {
        int expectedSize = (int) sessionList
                .stream()
                .filter(m -> testUser1.equals(m.getUser()))
                .count();
        int actualSize = sessionRepository.getSize(testUser1.getId());
        Assert.assertEquals(expectedSize, actualSize);
    }

    @Test
    @DisplayName("Удаление сессии")
    public void removeTest() {
        @Nullable final Session session = sessionList.get(1);
        Assert.assertNotNull(session);
        @NotNull final String sessionId = session.getId();
        @Nullable final Session deletedSession = sessionRepository.remove(session);
        Assert.assertNotNull(deletedSession);
        @Nullable final Session deletedSessionInRepository = sessionRepository.findOneById(sessionId);
        Assert.assertNull(deletedSessionInRepository);
    }

    @Test
    @DisplayName("Удаление Null сессии")
    public void removeNullTest() throws Exception {
        @Nullable final Session session = sessionRepository.remove(null);
        Assert.assertNull(session);
    }

}