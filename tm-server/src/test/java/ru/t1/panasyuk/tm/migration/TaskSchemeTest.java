package ru.t1.panasyuk.tm.migration;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;
import ru.t1.panasyuk.tm.repository.dto.TaskDtoRepository;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;

@DisplayName("Тестирование создания схемы для задач")
public class TaskSchemeTest extends AbstractSchemeTest {

    @Test
    @DisplayName("Создание схемы")
    public void test() throws LiquibaseException {
        liquibase.update("task");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        @NotNull final TaskDTO task = createTask();
        Assert.assertNotNull(task);
        int countOfTasks = getCountOfTasks();
        Assert.assertEquals(1, countOfTasks);
        deleteTask(task);
    }

    @NotNull
    private TaskDTO createTask() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
        @NotNull final TaskDTO taskDTO = new TaskDTO();
        taskRepository.add(taskDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return taskDTO;
    }

    private int getCountOfTasks() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
        final int count = taskRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteTask(@NotNull final TaskDTO task) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final ITaskDtoRepository taskRepository = new TaskDtoRepository(entityManager);
        taskRepository.remove(task);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}