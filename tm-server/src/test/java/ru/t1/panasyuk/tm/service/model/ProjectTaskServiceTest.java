package ru.t1.panasyuk.tm.service.model;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.api.service.model.IProjectService;
import ru.t1.panasyuk.tm.api.service.model.IProjectTaskService;
import ru.t1.panasyuk.tm.api.service.model.ITaskService;
import ru.t1.panasyuk.tm.api.service.model.IUserService;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.exception.entity.ProjectNotFoundException;
import ru.t1.panasyuk.tm.exception.entity.TaskNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.exception.field.ProjectIdEmptyException;
import ru.t1.panasyuk.tm.exception.field.TaskIdEmptyException;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.model.User;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@DisplayName("Тестирование сервиса ProjectTaskService")
public class ProjectTaskServiceTest extends AbstractSchemeTest {

    @NotNull
    private List<Project> projectList;

    @NotNull
    private IProjectService projectService;

    @NotNull
    private ITaskService taskService;

    @NotNull
    private IProjectTaskService projectTaskService;

    @NotNull
    private IUserService userService;

    @NotNull
    private User test;

    @NotNull
    private User admin;

    @BeforeClass
    public static void initConnection() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
    }

    @Before
    public void initService() throws Exception {
        @NotNull final IPropertyService propertyService = new PropertyService();
        projectService = new ProjectService(connectionService);
        taskService = new TaskService(connectionService);
        projectTaskService = new ProjectTaskService(connectionService);
        userService = new UserService(propertyService, connectionService);
        projectList = new ArrayList<>();
        test = userService.create("TEST", "TEST", "TEST@TEST.ru");
        admin = userService.create("ADMIN", "ADMIN", "ADMIN@TEST.ru", Role.ADMIN);
        @NotNull final Project project1 = projectService.create(test.getId(), "Project 1", "Project for TEST");
        @NotNull final Project project2 = projectService.create(admin.getId(), "Project 2", "Project for ADMIN");
        @NotNull final Project project3 = projectService.create(test.getId(), "Project 3", "Project for TEST 2");
        @NotNull final Task task1 = taskService.create(test.getId(), "Task 1", "Task for project 1");
        task1.setProject(project1);
        taskService.update(task1);
        @NotNull final Task task2 = taskService.create(test.getId(), "Task 2", "Task for project 1");
        task2.setProject(project3);
        taskService.update(task2);
        @NotNull final Task task3 = taskService.create(admin.getId(), "Task 3", "Task for project 2");
        task3.setProject(project2);
        taskService.update(task3);
        @NotNull final Task task4 = taskService.create(admin.getId(), "Task 4", "Task for project 2");
        task4.setProject(project2);
        taskService.update(task4);
        projectList.add(project1);
        projectList.add(project2);
        projectList.add(project3);
    }

    @After
    public void afterTest() throws Exception {
        taskService.clear(test.getId());
        taskService.clear(admin.getId());
        projectService.clear(test.getId());
        projectService.clear(admin.getId());
        userService.remove(admin);
        userService.remove(test);
    }

    @Test
    @DisplayName("Связать задачу с проектом")
    public void bindTaskToProjectTest() throws Exception {
        @Nullable final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @NotNull final Project project = projects.get(0);
        int expectedNumberOfEntries = taskService.findAllByProjectId(test.getId(), project.getId()).size() + 1;
        @NotNull final Task newTask = taskService.create(test.getId(), "Test task", "Task for test");
        @NotNull final Task boundTask = projectTaskService.bindTaskToProject(test.getId(), project.getId(), newTask.getId());
        Assert.assertNotNull(boundTask);
        int newNumberOfEntries = taskService.findAllByProjectId(test.getId(), project.getId()).size();
        Assert.assertEquals(expectedNumberOfEntries, newNumberOfEntries);
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Связать задачу с проектом по Null Id проекта")
    public void bindTaskToProjectProjectIdNullTestNegative() throws Exception {
        projectTaskService.bindTaskToProject(test.getId(), null, "TASK_ID");
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Связать задачу с проектом по пустому Id проекта")
    public void bindTaskToProjectProjectIdEmptyTestNegative() throws Exception {
        projectTaskService.bindTaskToProject(test.getId(), "", "TASK_ID");
    }

    @Test(expected = TaskIdEmptyException.class)
    @DisplayName("Связать задачу с проектом по Null Id задачи")
    public void bindTaskToProjectTaskIdNullTestNegative() throws Exception {
        projectTaskService.bindTaskToProject(test.getId(), "PROJECT_ID", null);
    }

    @Test(expected = TaskIdEmptyException.class)
    @DisplayName("Связать задачу с проектом по путсому Id задачи")
    public void bindTaskToProjectTaskIdEmptyTestNegative() throws Exception {
        projectTaskService.bindTaskToProject(test.getId(), "PROJECT_ID", "");
    }

    @Test(expected = ProjectNotFoundException.class)
    @DisplayName("Связать задачу с несуществующим проектом")
    public void bindTaskToProjectProjectNotFoundTestNegative() throws Exception {
        projectTaskService.bindTaskToProject(test.getId(), "PROJECT_ID", "123321");
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Связать несуществующую задачу с проектом")
    public void bindTaskToProjectTaskNotFoundTestNegative() throws Exception {
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.bindTaskToProject(test.getId(), projectId, "123321");
    }

    @Test
    @DisplayName("Удалить проект по Id")
    public void removeProjectByIdTest() throws Exception {
        @NotNull final List<Project> projects = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        for (@NotNull final Project project : projects) {
            @NotNull final Project deletedProject = projectTaskService.removeProjectById(test.getId(), project.getId());
            Assert.assertNotNull(deletedProject);
        }
        @Nullable final List<Project> projectsAfterRemoving = projectService.findAll(test.getId());
        Assert.assertNotNull(projectsAfterRemoving);
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @Nullable final List<Task> tasksAfterRemoving = taskService.findAll(test.getId());
        Assert.assertNotNull(tasksAfterRemoving);
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Удалить проект по Null Id")
    public void removeProjectByIdProjectIdNullTestNegative() throws Exception {
        projectTaskService.removeProjectById(test.getId(), null);
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Удалить проект по пустому Id")
    public void removeProjectByIdProjectIdEmptyTestNegative() throws Exception {
        projectTaskService.removeProjectById(test.getId(), "");
    }

    @Test(expected = ProjectNotFoundException.class)
    @DisplayName("Удалить несуществующий проект по Id")
    public void removeProjectByIdProjectNotFoundTestNegative() throws Exception {
        projectTaskService.removeProjectById(test.getId(), "123321");
    }

    @Test
    @DisplayName("Удалить проект по индексу")
    public void removeProjectByIndexTest() throws Exception {
        @NotNull final List<Project> projects = projectList
                .stream()
                .filter(m -> test.equals(m.getUser()))
                .collect(Collectors.toList());
        Assert.assertTrue(projects.size() > 0);
        int index = projects.size();
        while (index > 0) {
            @NotNull final Project deletedProject = projectTaskService.removeProjectByIndex(test.getId(), index);
            Assert.assertNotNull(deletedProject);
            index--;
        }
        @Nullable final List<Project> projectsAfterRemoving = projectService.findAll(test.getId());
        Assert.assertNotNull(projectsAfterRemoving);
        Assert.assertEquals(0, projectsAfterRemoving.size());
        @Nullable final List<Task> tasksAfterRemoving = taskService.findAll(test.getId());
        Assert.assertNotNull(tasksAfterRemoving);
        Assert.assertEquals(0, tasksAfterRemoving.size());
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить проект по Null индексу")
    public void removeProjectByIndexNullTestNegative() throws Exception {
        projectTaskService.removeProjectByIndex(test.getId(), null);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить проект по отрицательному индексу")
    public void removeProjectByIndexMinusTestNegative() throws Exception {
        projectTaskService.removeProjectByIndex(test.getId(), -1);
    }

    @Test(expected = IndexIncorrectException.class)
    @DisplayName("Удалить проект по индексу превышающему количество задач")
    public void removeProjectByIndexTestNegative() throws Exception {
        projectTaskService.removeProjectByIndex(test.getId(), projectList.size() + 1);
    }

    @Test
    @DisplayName("Удалить все проекты")
    public void clearProjectTest() throws Exception {
        int numberOfProjects = projectService.getSize(test.getId());
        Assert.assertTrue(numberOfProjects > 0);
        int numberOfTasks = taskService.getSize(test.getId());
        Assert.assertTrue(numberOfTasks > 0);
        projectTaskService.clearProjects(test.getId());
        int numberOfProjectsAfterRemoving = projectService.getSize(test.getId());
        Assert.assertEquals(0, numberOfProjectsAfterRemoving);
        int numberOfTasksAfterRemoving = taskService.getSize(test.getId());
        Assert.assertEquals(0, numberOfTasksAfterRemoving);
    }

    @Test
    @DisplayName("Отвязать задачу от проекта")
    public void unbindTaskFromProjectTest() throws Exception {
        @Nullable final List<Project> projects = projectService.findAll(test.getId());
        Assert.assertNotNull(projects);
        Assert.assertTrue(projects.size() > 0);
        @NotNull final Project project = projects.get(0);
        @NotNull final List<Task> tasks = taskService.findAllByProjectId(test.getId(), project.getId());
        @NotNull final String taskForUnbindId = tasks.get(0).getId();
        @Nullable final Task boundTask = projectTaskService.bindTaskToProject(test.getId(), project.getId(), taskForUnbindId);
        Assert.assertNotNull(boundTask);
        @Nullable Task unboundTask = projectTaskService.unbindTaskFromProject(test.getId(), boundTask.getProject().getId(), boundTask.getId());
        Assert.assertNotNull(unboundTask);
        unboundTask = taskService.findOneById(test.getId(), unboundTask.getId());
        Assert.assertNotNull(unboundTask);
        Assert.assertNull(unboundTask.getProject());
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Отвязать задачу от проекта по Null Id проекта")
    public void unbindTaskFromProjectProjectIdNullTestNegative() throws Exception {
        projectTaskService.unbindTaskFromProject(test.getId(), null, "TASK_ID");
    }

    @Test(expected = ProjectIdEmptyException.class)
    @DisplayName("Отвязать задачу от проекта по пустому Id проекта")
    public void unbindTaskFromProjectProjectIdEmptyTestNegative() throws Exception {
        projectTaskService.unbindTaskFromProject(test.getId(), "", "TASK_ID");
    }

    @Test(expected = TaskIdEmptyException.class)
    @DisplayName("Отвязать задачу от проекта по Null Id задачи")
    public void unbindTaskFromProjectTaskIdNullTestNegative() throws Exception {
        projectTaskService.unbindTaskFromProject(test.getId(), "PROJECT_ID", null);
    }

    @Test(expected = TaskIdEmptyException.class)
    @DisplayName("Отвязать задачу от проекта по пустому Id задачи")
    public void unbindTaskFromProjectTaskIdEmptyTestNegative() throws Exception {
        projectTaskService.unbindTaskFromProject(test.getId(), "PROJECT_ID", "");
    }

    @Test(expected = ProjectNotFoundException.class)
    @DisplayName("Отвязать задачу от несуществующего проекта")
    public void unbindTaskFromProjectProjectNotFoundTestNegative() throws Exception {
        projectTaskService.unbindTaskFromProject(test.getId(), "PROJECT_ID", "123321");
    }

    @Test(expected = TaskNotFoundException.class)
    @DisplayName("Отвязать несуществующую задачу от проекта")
    public void unbindTaskFromProjectTaskNotFoundTestNegative() throws Exception {
        @NotNull final String projectId = projectList.get(0).getId();
        projectTaskService.unbindTaskFromProject(test.getId(), projectId, "123321");
    }

}