package ru.t1.panasyuk.tm.migration;

import io.qameta.allure.junit4.DisplayName;
import liquibase.exception.LiquibaseException;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import ru.t1.panasyuk.tm.AbstractSchemeTest;
import ru.t1.panasyuk.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.repository.dto.ProjectDtoRepository;
import ru.t1.panasyuk.tm.service.ConnectionService;
import ru.t1.panasyuk.tm.service.PropertyService;

import javax.persistence.EntityManager;

@DisplayName("Тестирование создания схемы")
public class SchemeTest extends AbstractSchemeTest {

    @Test
    @DisplayName("Создание схемы")
    public void test() throws LiquibaseException {
        liquibase.update("scheme");
        @NotNull final IPropertyService propertyService = new PropertyService();
        connectionService = new ConnectionService(propertyService);
        @NotNull final ProjectDTO project = createProject();
        Assert.assertNotNull(project);
        int countOfProjects = getCountOfProjects();
        Assert.assertEquals(1, countOfProjects);
        deleteProject(project);
    }

    @NotNull
    private ProjectDTO createProject() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
        @NotNull final ProjectDTO projectDTO = new ProjectDTO();
        projectRepository.add(projectDTO);
        entityManager.getTransaction().commit();
        entityManager.close();
        return projectDTO;
    }

    private int getCountOfProjects() {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
        final int count = projectRepository.getSize();
        entityManager.getTransaction().commit();
        entityManager.close();
        return count;
    }

    private void deleteProject(@NotNull final ProjectDTO project) {
        @NotNull final EntityManager entityManager = connectionService.getEntityManager();
        entityManager.getTransaction().begin();
        @NotNull final IProjectDtoRepository projectRepository = new ProjectDtoRepository(entityManager);
        projectRepository.remove(project);
        entityManager.getTransaction().commit();
        entityManager.close();
    }

}