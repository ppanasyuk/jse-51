package ru.t1.panasyuk.tm.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.constant.DBConst;
import ru.t1.panasyuk.tm.enumerated.Role;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = DBConst.TABLE_USER)
public final class User extends AbstractModel {

    private static final long serialVersionUID = 1;

    @Nullable
    @Column(name = DBConst.COLUMN_LOGIN)
    private String login;

    @Nullable
    @Column(name = DBConst.COLUMN_PASSWORD)
    private String passwordHash;

    @Nullable
    @Column(name = DBConst.COLUMN_EMAIL)
    private String email;

    @Nullable
    @Column(name = DBConst.COLUMN_LAST_NAME)
    private String lastName;

    @Nullable
    @Column(name = DBConst.COLUMN_FIRST_NAME)
    private String firstName;

    @Nullable
    @Column(name = DBConst.COLUMN_MIDDLE_NAME)
    private String middleName;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = DBConst.COLUMN_ROLE, length = 30)
    private Role role = Role.USUAL;

    @NotNull
    @Column(name = DBConst.COLUMN_LOCKED)
    private Boolean locked = false;

    @Nullable
    @JsonManagedReference(value = "user-tasks")
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Task> tasks = new ArrayList<>();

    @Nullable
    @JsonManagedReference(value = "user-projects")
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Project> projects = new ArrayList<>();

    @Nullable
    @JsonManagedReference(value = "user-sessions")
    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Session> sessions = new ArrayList<>();

    @Override
    public String toString() {
        return String.format(
                "Login=%s\nLast Name=%s\nFirst Name=%s\nMiddle Name=%s\nEmail=%s\nRole=%s",
                login, lastName, firstName, middleName, email, role.getDisplayName()
        );
    }

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(@NotNull final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof User)) return false;
        @NotNull final User user = (User) obj;
        return user.getId().equals(this.getId());
    }

}