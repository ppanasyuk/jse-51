package ru.t1.panasyuk.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.response.AbstractResultResponse;

@NoArgsConstructor
public abstract class AbstractProjectResponse extends AbstractResultResponse {

    public AbstractProjectResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}