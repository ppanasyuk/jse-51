package ru.t1.panasyuk.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskUpdateByIndexRequest extends AbstractUserRequest {

    @Nullable
    private Integer index;

    @Nullable
    private String name;

    @Nullable
    private String description;

    public TaskUpdateByIndexRequest(
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        this.index = index;
        this.name = name;
        this.description = description;
    }

    public TaskUpdateByIndexRequest(
            @Nullable final String token,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        super(token);
        this.index = index;
        this.name = name;
        this.description = description;
    }

}