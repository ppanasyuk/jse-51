package ru.t1.panasyuk.tm.service;

import lombok.Cleanup;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.IPropertyService;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import static java.lang.ClassLoader.getSystemResourceAsStream;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String ACTIVE_MQ_HOST_KEY = "activeMQ.host";

    @NotNull
    public static final String ACTIVE_MQ_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String ACTIVE_MQ_PORT_KEY = "activeMQ.port";

    @NotNull
    public static final String ACTIVE_MQ_PORT_DEFAULT = "61616";

    @NotNull
    private static final String APPLICATION_FILENAME_KEY = "application.config";

    @NotNull
    private static final String APPLICATION_FILENAME_DEFAULT = "application.properties";

    @NotNull
    public static final String MONGO_CLIENT_HOST_KEY = "mongo.client.host";

    @NotNull
    public static final String MONGO_CLIENT_HOST_DEFAULT = "localhost";

    @NotNull
    public static final String MONGO_CLIENT_PORT_KEY = "mongo.client.port";

    @NotNull
    public static final String MONGO_CLIENT_PORT_DEFAULT = "localhost";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        final boolean existsConfig = isExistsExternalConfig();
        if (existsConfig) loadExternalConfig(properties);
        else loadInternalConfig(properties);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace('.', '_').toUpperCase();
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().containsKey(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    private boolean isExistsExternalConfig() {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        return file.exists();
    }

    @SneakyThrows
    @SuppressWarnings("IOStreamConstructor")
    private void loadExternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = getApplicationConfig();
        @NotNull final File file = new File(name);
        @Cleanup @Nullable final InputStream inputStream = new FileInputStream(file);
        properties.load(inputStream);
    }

    @SneakyThrows
    private void loadInternalConfig(@NotNull final Properties properties) {
        @NotNull final String name = APPLICATION_FILENAME_DEFAULT;
        @Cleanup @Nullable final InputStream inputStream = getSystemResourceAsStream(name);
        if (inputStream == null) return;
        properties.load(inputStream);
    }

    @NotNull
    @Override
    public String getActiveMqHost() {
        return getStringValue(ACTIVE_MQ_HOST_KEY, ACTIVE_MQ_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getActiveMqPort() {
        return getStringValue(ACTIVE_MQ_PORT_KEY, ACTIVE_MQ_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getApplicationConfig() {
        return getStringValue(APPLICATION_FILENAME_KEY, APPLICATION_FILENAME_DEFAULT);
    }

    @NotNull
    @Override
    public String getMongoClientHost() {
        return getStringValue(MONGO_CLIENT_HOST_KEY, MONGO_CLIENT_HOST_DEFAULT);
    }

    @Override
    public @NotNull String getMongoClientPort() {
        return getStringValue(MONGO_CLIENT_PORT_KEY, MONGO_CLIENT_PORT_DEFAULT);
    }

}